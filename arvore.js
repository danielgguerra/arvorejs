class Arvore {
    constructor() {
        this.raiz = null;
        this.quantidade = 0;
    }

    get getRaiz() {
        return this.raiz;
    }

    get getQuantidade() {
        return this.quantidade;
    }

    set setRaiz(_raiz) {
        this.raiz = _raiz
    }
    
    set setQuantidade(_quantidade) {
        this.quantidade = _quantidade;
    }

    inserir(_valor) {

        if(this.getRaiz == null) {

            this.setRaiz = new No(_valor)
            this.quantidade += 1

            return true
        
        } else {

            if(!this.pesquisa(_valor)) {
                
                this.inserirNo(this.getRaiz, _valor)
                this.quantidade += 1;
                
                return true
            }

        }

        return false
    }

    inserirNo(_raiz, _valor){

        if(_raiz.getObjeto.getValor > _valor) {

            if(_raiz.getEsquerda === null) {
                _raiz.setEsquerda = new No(_valor)
            } else {
                this.inserirNo(_raiz.getEsquerda, _valor)
            }

        } else {

            if(_raiz.getDireita === null) {
                _raiz.setDireita = new No(_valor)
            } else {
                this.inserirNo(_raiz.getDireita, _valor)
            }

        }
    }

    pesquisa(_valor) {

        if(this.getRaiz !== null) {

            return this.pesquisaNo(this.getRaiz, _valor)

        } else {
            return false;
        }
        
    }

    pesquisaNo(_raiz, _valor) {

        if(_raiz == null) {
            
            return null

        } else {

            if(_raiz.getObjeto.getValor == _valor){

                return _raiz

            } else {
    
                if(_raiz.getObjeto.getValor > _valor) {

                    return this.pesquisaNo(_raiz.getEsquerda, _valor)

                } else {

                    return this.pesquisaNo(_raiz.getDireita, _valor)

                }
            }
        }

    }


    remove(_valor) {

        let noRemover = this.pesquisa(_valor)

        if(noRemover != null) {

            if(this.getQuantidade == 1) {

                this.setRaiz = null
                this.quantidade -= 1
                return true

            } else {

                let noPai = this.getPai(_valor)

                // Caso: Remover nó folha
                if(noRemover.getEsquerda == null && noRemover.getDireita == null) {

                    if(noPai.getObjeto.getValor > _valor) {
                        
                        noPai.setEsquerda = null
                        this.quantidade -= 1
                        return true
                    
                    } else {
    
                        noPai.setDireita = null
                        this.quantidade -= 1
                        return true
    
                    }

                } 
                // Caso: Remover nó com dois filhos
                else if(noRemover.getEsquerda != null && noRemover.getDireita != null) {
                    // O valor que irá substituir o valor do nó removido tem que ser
                    // o nó mais a esquerda do filho direito do nó removido
                    let assumeNoRemovido = this.getMaisAEsquerda(noRemover.getDireita)
                    this.remove(assumeNoRemovido.getObjeto.getValor)

                    noRemover.getObjeto.setValor = assumeNoRemovido.getObjeto.getValor

                    return true

                } else {
                    // Caso: Nó a remove com pelo menos um filho
                    let assumeNoRemovido;

                    if(noRemover.getEsquerda == null) {
                        assumeNoRemovido = noRemover.getDireita
                    } else {
                        assumeNoRemovido = noRemover.getEsquerda
                    }

                    if(this.raiz == assumeNoRemovido) {
                        this.raiz.setRaiz = assumeNoRemovido
                    } else if(noPai.getEsquerda == noRemover) {
                        noPai.setEsquerda = assumeNoRemovido
                    } else if(noPai.getDireita == noRemover) {
                        noPai.setDireita = assumeNoRemovido
                    }
                    
                    return true
                }

            }

        } else {

            return false
        
        }

    }

    getMaisAEsquerda(_raiz) {

        if(_raiz.getEsquerda != null) {

            return this.getMaisAEsquerda(_raiz.getEsquerda)

        } else {
            return _raiz
        }
    }

    getPai(_valorFilho) {

        if(this.getRaiz != null) {
            return this.getPaiAux(this.getRaiz, _valorFilho)
        } else {
            return null
        }

    }

    getPaiAux(_raiz, _valor) {

        if(_raiz.getObjeto.getValor > _valor) {

            if(_raiz.getEsquerda == null) {
                return null
            } else if(_raiz.getEsquerda.getObjeto.getValor == _valor) {
                return _raiz
            } else {
                return this.getPaiAux(_raiz.getEsquerda, _valor)
            }

        } else {

            if(_raiz.getDireita == null) {
                return null
            } else if(_raiz.getDireita.getObjeto.getValor == _valor) {
                return _raiz
            } else {
                return this.getPaiAux(_raiz.getDireita, _valor)
            }

        }
    }

    emOrder() {
        
        this.emOrderAux(this.getRaiz)
    }

    emOrderAux(_raiz) {

        if(_raiz != null) {
            
            this.emOrderAux(_raiz.getEsquerda)
            console.log(_raiz.getObjeto.getValor)
            this.emOrderAux(_raiz.getDireita)

        }

    }

    preOrder() {

        this.preOrderAux(this.getRaiz)

    }

    preOrderAux(_raiz) {

        if(_raiz != null) {

            console.log(_raiz.getObjeto.getValor)
            this.preOrderAux(_raiz.getEsquerda)
            this.preOrderAux(_raiz.getDireita)

        }

    }

    posOrder() {

        this.posOrderAux(this.getRaiz)

    }

    posOrderAux(_raiz) {

        if(_raiz != null) {

            this.posOrderAux(_raiz.getEsquerda)
            this.posOrderAux(_raiz.getDireita)
            console.log(_raiz.getObjeto.getValor)

        }

    }

}

class No {

    constructor(_valor) {
        this.objValor = new Objeto(_valor);
        this.esquerda = null;
        this.direita = null;
    }

    get getObjeto() {
        return this.objValor;
    }

    get getEsquerda() {
        return this.esquerda;
    }

    get getDireita() {
        return this.direita;
    }

    set setObjeto(_valor) {
        this.objValor = _valor
    }

    set setEsquerda(_esquerda) {
        this.esquerda = _esquerda
    }

    set setDireita(_direita) {
        this.direita = _direita
    }

}

class Objeto {

    constructor(_valor){
        this.valor = _valor;
    }

    get getValor() {
        return this.valor;
    }

    set setValor(_novoValor) {
        this.valor = _novoValor;
    }
}

let a = new Arvore()

a.inserir(10)
a.inserir(5)
a.inserir(15)
a.inserir(11)
a.inserir(17)
a.inserir(16)
a.inserir(3)
a.inserir(1)
a.inserir(4)

// console.log(a.remove(100))
// console.log('------------')
console.log(a.posOrder())

// console.log(a.remove(3))

// console.log(a.emOrder())
